package br.com.jms.queue;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

public class TestProducer {
	
	public static void main(String[] args) throws Exception {
		
		InitialContext context = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
				
		Connection connection = factory.createConnection();
		connection.start();
		
		Session sessao = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Destination file = (Destination) context.lookup("LOG");
		
		MessageProducer producer = sessao.createProducer(file);
		
		Message message = sessao.createTextMessage("LOG Problema de LOCK");
		
		// prioridade da msg 0 menor 9 maior prioridade, 3 parametro
		producer.send(message, DeliveryMode.NON_PERSISTENT,0, 80000);
		
		sessao.close();
		connection.close();
		context.close();
		
	}
}
