package com.br.jms;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

import javax.jms.ConnectionFactory;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.jms.Destination;

public class ConectorJms {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
	private InitialContext context;
	private Connection conexao;
	private Destination fila;

	public ConectorJms() {
		try {
			this.context = createInitialContext();
			ConnectionFactory cf = (ConnectionFactory) this.context.lookup("ConnectionFactory");
			this.conexao = (Connection) cf.createConnection();
			this.fila = (Destination) this.context.lookup("produto");

		} catch (JMSException e) {

			throw new RuntimeException(e);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InitialContext createInitialContext() throws NamingException {
		Properties props = new Properties();
		props.setProperty("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		props.setProperty("java.naming.provider.url", "tcp://0.0.0.0:61616");
		props.setProperty("queue.produto", "produto");

		InitialContext ctx = new InitialContext(props);
		return ctx;
	}

	public void consomeMensagens() {
		try {
			Session sessao = this.conexao.createSession(false, 1);

			MessageConsumer consumer = sessao.createConsumer(this.fila);

			this.conexao.start();

			consumer.setMessageListener(new MessageListener() {

				public void onMessage(Message message) {

					TextMessage textMessage = (TextMessage) message;

					try {
						System.out.println("Recebendo mensagem: " + textMessage.getText());
					} catch (JMSException e) {
						throw new RuntimeException((Throwable) e);
					}
				}
			});

			aguardeEnter();

			sessao.close();

		} catch (JMSException e) {
			throw new RuntimeException((Throwable) e);
		}
	}

	private void aguardeEnter() {
		System.out.println("Aperte enter para finalizar");
		Scanner s = new Scanner(System.in);
		s.nextLine();
		s.close();
	}

	private String geraHora() {
		return SDF.format(new Date());
	}

	public void enviaMensagens(int max) {
		try {
			Session sessao = ((javax.jms.Connection) this.conexao).createSession(false, 1);
			MessageProducer producer = sessao.createProducer(this.fila);

			System.out.println("Enviando " + max + " menagem(ns)");

			for (int i = 0; i < max; i++) {
				TextMessage textMessage = sessao.createTextMessage("Mensagem " + (i + 1) + " (" + geraHora() + ")");
				producer.send((Message) textMessage);
			}

			sessao.close();
		} catch (JMSException e) {
			throw new RuntimeException((Throwable) e);
		}
	}

	public void close() {
		try {
			this.conexao.close();
			this.context.close();
		} catch (NamingException e) {
			throw new RuntimeException(e);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}