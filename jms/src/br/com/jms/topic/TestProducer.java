package br.com.jms.topic;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

import br.com.jms.modelo.Pedido;
import br.com.jms.modelo.PedidoFactory;

public class TestProducer {
	
	public static void main(String[] args) throws Exception {
		
		InitialContext context = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
				
		Connection connection = factory.createConnection();
		connection.start();
		
		Session sessao = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Destination topico = (Destination) context.lookup("produto.disponivel");
		
		MessageProducer producer = sessao.createProducer(topico);
		
		Pedido pedido = new PedidoFactory().geraPedidoComValores();
		
		Message message = sessao.createObjectMessage(pedido);
		
		producer.send(message);		
		
		sessao.close();
		connection.close();
		context.close();
		
	}
}
