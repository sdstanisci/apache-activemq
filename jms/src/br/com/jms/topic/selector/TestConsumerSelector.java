package br.com.jms.topic.selector;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.InitialContext;

public class TestConsumerSelector {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		InitialContext context = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
				
		Connection connection = factory.createConnection();
		connection.setClientID("cartoes");
		
		connection.start();
		
		Session sessao = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Topic topico = (Topic) context.lookup("produto.disponivel");
		
		MessageConsumer consumer = sessao.createDurableSubscriber((Topic) topico, "assinaturaDuravel");
		
		consumer.setMessageListener(new MessageListener() {
			
			@Override
			public void onMessage(Message menssage) {
				
				TextMessage textMessage = (TextMessage) menssage;
		
				System.out.println("Msg no topico cartoes: " + menssage);

				try {
					System.out.println(textMessage.getText());
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
			
		new Scanner(System.in).next();
		
		sessao.close();
		connection.close();
		context.close();
		
	}
}
