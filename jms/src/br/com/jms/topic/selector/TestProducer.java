package br.com.jms.topic.selector;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

public class TestProducer {
	
	public static void main(String[] args) throws Exception {
		
		InitialContext context = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
				
		Connection connection = factory.createConnection();
		connection.start();
		
		Session sessao = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Destination topico = (Destination) context.lookup("produto.disponivel");
		
		MessageProducer producer = sessao.createProducer(topico);
		
		Message message = sessao.createTextMessage(
				"<proposta>\n" + 
				"   <id>12345</id>\n" + 
				"   <produtoValido>false</produtoValido>\n" +
				"   <cliente>Sergio Stanisci</cliente>\n" + 
				"</proposta>\n");
		
		
		message.setBooleanProperty("produtoValido", true);
		
		producer.send(message);		
		
		sessao.close();
		connection.close();
		context.close();
		
	}
}
