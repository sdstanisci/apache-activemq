package br.com.jms.topic;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.InitialContext;

import br.com.jms.modelo.Pedido;

public class TestConsumerProdutoCartoes {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		System.setProperty("org.apache.activemq.SERIALIZABLE_PACKAGES","*"); 
		
		InitialContext context = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
				
		Connection connection = factory.createConnection();
		connection.setClientID("cartoes");
		
		connection.start();
		
		Session sessao = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Topic topico = (Topic) context.lookup("produto.disponivel");
		
		MessageConsumer consumer = sessao.createDurableSubscriber((Topic) topico, "assinaturaDuravel");
		
		consumer.setMessageListener(new MessageListener() {
			
			@Override
			public void onMessage(Message menssage) {
				
				ObjectMessage objectMessage = (ObjectMessage) menssage;
				
				System.out.println("Msg no topico de produto - consumidor cartoes: " + objectMessage);

				try {
					Pedido pedido = (Pedido) objectMessage.getObject();
					
					System.out.println(pedido.getCodigo());
				
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
			
		new Scanner(System.in).next();
		
		sessao.close();
		connection.close();
		context.close();
		
	}
}
